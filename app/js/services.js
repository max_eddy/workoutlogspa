'use strict';

//  Example of Storage Layout
// [
//     {
//         name: 'Foo',
//         restLength: 60,
//         exercise: [
//             { name: 'foo', numSets: 3 },
//             { name: 'bar', numSets: 1 },
//             { name: 'baz', numSets: 4 }
//         ],
//         workouts: [{
//             exercises: [
//                 {
//                     name: 'foo',
//                     numSets: 3,
//                     sets: [
//                         { completed: '*Date String*', reps: 8, weight: 180 },
//                         .. etc
//                     ]
//                 },
//                 {
//                     name: 'bar',
//                     numSets: 1,
//                     sets: [ ... ]
//                 },
//                 {
//                     name: 'baz',
//                     numSets: 4,
//                     sets: [ ... ]
//                 }
//             ]
//         }]
//     }
// ]

// Wrap ES6 promises with angular's Q promise library for better compatibility.
app.service('storageService', ['$q', function($q) {
    var prefix = 'wl';

    this.setPrefix = function(newPrefix) {
        prefix = newPrefix;
    };

    this.getPrefix = function() {
        return prefix;
    };

    this.persist = function(name, value) {
        var deferred = $q.defer();
        value = angular.toJson(value);
        value = $.parseJSON(value);
        localforage.setItem(prefix + name, value).then(function() {
            deferred.resolve();
        });
        return deferred.promise;
    };

    this.find = function(name) {
        var deferred = $q.defer();
        localforage.getItem(prefix + name).then(function(item) {
            deferred.resolve(item);
        });
        return deferred.promise;
    };

    this.delete = function(name) {
        var deferred = $q.defer();
        localforage.removeItem(prefix + name).then(function() {
            deferred.resolve();
        });
        return deferred.promise;
    };
}]);

app.factory('routineFactory', ['workoutFactory', function(Workout) {
    function Routine(data) {
        data = data || {};

        this.name = data.name || '';
        this.exercises = data.exercises || [];
        this.restLength = data.restLength || 60;
        this.workouts = (data.workouts || []).map(function (workoutData) {
            return new Workout(workoutData.exercises, workoutData.date);
        });
    }

    Routine.prototype.newExercise = function() {
        this.exercises.push({ name: '', numSets: 1 });
    };

    Routine.prototype.removeExercise = function(index) {
        this.exercises.splice(index, 1);
    };

    Routine.prototype.newWorkout = function() {
        return this.workouts.push(new Workout(this.exercises));
    };

    Routine.prototype.deleteWorkout = function(index) {
        this.workouts.splice(index, 1);
    };

    return Routine;
}]);

app.factory('workoutFactory', function() {
    function Workout(exercises, date) {
        var i,
            j;

        this.exercises = exercises;
        for (i = 0; i < exercises.length; i += 1) {
            // This is a pre-existing workout that has already been setup.
            if (this.exercises[i].sets) {
                continue;
            }

            this.exercises[i].sets = [];
            for (j = 0; j < exercises[i].numSets; j+= 1) {
                this.exercises[i].sets.push({
                    completed: false,
                    reps: 0,
                    weight: 0
                });
            }
        }
        this.date = date ? new Date(date) : new Date();
    }

    Workout.prototype.setEnabled = function(index) {
        return index !== 0;
    };

    Workout.prototype.prettyDate = function() {
        if (!this.date instanceof Date) {
            return;
        }

        return this.date.getFullYear() + '-' +
            (this.date.getMonth() < 10 ? '0' + this.date.getMonth() : this.date.getMonth()) + '-' +
            (this.date.getDate() < 10 ? '0' + this.date.getDate() : this.date.getDate()) + ' ' +
            this.date.getHours() + ':' +
            (this.date.getMinutes() < 10 ? '0' + this.date.getMinutes() : this.date.getMinutes());
    };

    Workout.prototype.progress = function() {
        var i,
            total = 0,
            completed = 0,
            isCompleted = function(set) {
                return set instanceof Date;
            };

        for (i = 0; i < this.exercises.length; i += 1) {
            total += this.exercises[i].sets.length;
            completed += this.exercises[i].sets.filter(isCompleted).length;
        }

        return Math.round(total * 100 / completed);
    };

    return Workout;
});