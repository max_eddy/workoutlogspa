'use strict';

app.directive('wlUnique', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, ngModel) {
            var property = ngModel.$name,
                collectionName = attributes.wlUnique;

            function valueExists(value) {
                var i,
                    models = scope.$eval(collectionName);

                // Avoid comparing value against its self.
                if (ngModel.$modelValue === value) {
                    return false;
                }

                // Avoid checking if there aren't any other values.
                if (typeof models === 'undefined') {
                    return false;
                }

                for (i = 0; i < models.length; i += 1) {
                    if (
                        models[i].hasOwnProperty(property) &&
                        typeof models[i][property] !== 'undefined' &&
                        typeof value !== 'undefined' &&
                        models[i][property].toLowerCase() === value.toLowerCase()
                    ) {
                        return true;
                    }
                }

                return false;
            }

            ngModel.$parsers.unshift(function(value) {
                var valid = !valueExists(value);
                ngModel.$setValidity('unique', valid);
                return valid ? value : undefined;
            });

            ngModel.$formatters.unshift(function(value) {
                ngModel.$setValidity('unique', !valueExists(value));
                return value;
            });
        }
    };
});

app.directive('wlMessages', function() {
    return {
        templateUrl: '/app/partials/messages.html'
    };
});