'use strict';

app.controller('RoutineController', [
    '$scope',
    '$q',
    '$routeParams',
    '$timeout',
    'storageService',
    'routineFactory',
    function ($scope, $q, $routeParams, $timeout, storageService, Routine) {
        var redirect;
        $scope.currentRoutine = null;
        $scope.currentWorkout = null;
        $scope.currentExercises = null;
        $scope.messages = [];
        $scope.routeParams = $routeParams;
        $scope.routineExists = angular.isDefined($routeParams.routineIndex);
        $scope.workoutExists = angular.isDefined($routeParams.workoutIndex);
        $scope.backup = null;
        $scope.timerPosition = 0;

        function save() {
            return $q.all([
                storageService.persist('routines', $scope.routines),
                storageService.persist('backup', $scope.backup)
            ]);
        }

        function displayMessage(type, text) {
            if (type !== 'success' && type !== 'info' && type !== 'warning' && type !== 'danger') {
                type = 'success';
            }
            $scope.messages.push({type: type, text: text});
        }

        function loadRoutines(data) {
            $scope.routines = [];
            data = data || [];
            for (var i = 0; i < data.length; i += 1) {
                $scope.routines.push(new Routine(data[i]));
            }
        }

        $scope.clearMessage = function(index) {
            $scope.messages.splice(index, 1);
        };

        $scope.saveRoutine = function() {
            if ($scope.routineForm.$invalid) {
                $scope.routineForm.$setDirty();
                return;
            }
            // Create new routine
            if (!angular.isDefined($routeParams.routineIndex)) {
                $scope.routines.push($scope.currentRoutine);
            }
            $scope.backup = null;

            save().then(function() {
                if (!angular.isDefined($routeParams.routineIndex)) {
                    window.location.href = '/app/routines/' + ($scope.routines.length - 1) + '/edit';
                } else {
                    displayMessage('success', 'The routine was successfully updated.');
                }
            });
        };

        $scope.deleteRoutine = function() {
            if (!angular.isDefined($routeParams.routineIndex)) {
                return;
            }

            $scope.backup = $scope.routines.slice(0);
            $scope.routines.splice($routeParams.routineIndex, 1).pop();
            save().then(function() {
                window.location.href = '/app/routines';
            });
        };

        $scope.undo = function() {
            if (!$scope.backup) {
                return;
            }
            loadRoutines($scope.backup);
            $scope.backup = null;
            save();
        };

        $scope.beginWorkout = function() {
            var index = $scope.currentRoutine.newWorkout() - 1;
            redirect = '/app/routines/' + $routeParams.routineIndex + '/workouts/' + index;
        };

        $scope.range = function(n) {
            return new Array(n);
        };

        $scope.finishSet = function(exercise, setIndex) {
            $scope.timerPosition = 0;
            function countdown(numSecs) {
                $timeout(function() {
                    $scope.timerPosition += 1;
                    numSecs -= 1;
                    if (numSecs > 0) {
                        countdown(numSecs);
                    } else {
                        var audio = new Audio('/app/timer.mp3');
                        audio.play();
                        exercise.sets[setIndex].completed = new Date();
                        save();
                        $timeout(function() {
                            $scope.timerPosition = 0;
                        }, 1000);
                    }
                }, 1000);
            }
            countdown($scope.currentRoutine.restLength);
        };

        $q.all([
            storageService.find('routines'),
            storageService.find('backup')
        ]).then(function (data) {
            loadRoutines(data[0]);
            $scope.backup = data[1] || [];

            // Check if specific routine is specified.
            if (!$scope.routineExists) {
                return;
            }

            if (!angular.isDefined($scope.routines[$routeParams.routineIndex])) {
                window.location.href = '/app/routines';
                return;
            } else {
                $scope.currentRoutine = $scope.routines[$routeParams.routineIndex];
                $scope.currentExercises = $scope.currentRoutine.exercises;
            }

            // Check if specific workout is specified.
            if ($scope.workoutExists) {
                if (!angular.isDefined($scope.currentRoutine.workouts[$routeParams.workoutIndex])) {
                    window.location.href = '/app/routines/' + $routeParams.routineIndex;
                    return;
                } else {
                    $scope.currentWorkout = $scope.currentRoutine.workouts[$routeParams.workoutIndex];
                    $scope.currentExercises = $scope.currentWorkout.exercises;
                }
            }

            if (!$scope.currentRoutine.exercises.length) {
                displayMessage('warning', 'This routine has no exercises defined.');
            }
            if (!$scope.currentRoutine.workouts.length) {
                displayMessage('info', 'No workouts have been logged yet. Start working out you lazy bum!');
            }

            // Track if workouts for this routine are added or removed.
            $scope.$watch('currentRoutine.workouts', function(newWorkouts, oldWorkouts) {
                if (newWorkouts.length !== oldWorkouts.length) {
                    save().then(function() {
                        if (redirect) {
                            window.location.href = redirect;
                        }
                    });
                }
            }, true);

            console.log($scope.currentWorkout);
        });
    }
]);

app.controller('NavbarController', ['$scope', '$location', function ($scope, $location) {
    $scope.isActive = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return true;
        } else {
            return false;
        }
    };
}]);