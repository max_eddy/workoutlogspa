'use strict';

var app = angular.module('workoutlog', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/app/routines', {
            controller: 'RoutineController',
            templateUrl: '/app/partials/routines.html'
        })
        .when('/app/routines/create', {
            controller: 'RoutineController',
            templateUrl: '/app/partials/routine-edit.html'
        })
        .when('/app/routines/:routineIndex', {
            controller: 'RoutineController',
            templateUrl: '/app/partials/routines.html'
        })
        .when('/app/routines/:routineIndex/edit', {
            controller: 'RoutineController',
            templateUrl: '/app/partials/routine-edit.html'
        })
        .when('/app/routines/:routineIndex/workouts/:workoutIndex', {
            controller: 'RoutineController',
            templateUrl: '/app/partials/workout-edit.html'
        })
        .otherwise({
            redirectTo: '/app/routines'
        });
}]);